'use strict';

angular.module('myApp.freebies', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/freebies', {
            templateUrl: 'templates/freebies/freebies.html',
            controller: 'FreebiesCtrl'
        });
    }])

    .controller('FreebiesCtrl', [function() {

    }]);