'use strict';

angular.module('myApp.step1', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/step1', {
        templateUrl: 'templates/step1/step1.html',
        controller: 'Step1Ctrl'
    });
}])

.controller('Step1Ctrl', [function ($scope, $http) {
    $('.ui.dropdown').dropdown();
}]);