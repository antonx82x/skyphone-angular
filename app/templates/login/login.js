'use strict';

angular.module('myApp.login', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/login', {
    templateUrl: 'templates/login/login.html',
    controller: 'LoginCtrl'
  });
}])

.controller('LoginCtrl', function($scope) {

  // function to submit the form after all validation has occurred
  $scope.submitForm = function() {

    // check to make sure the form is completely valid
    if ($scope.loginForm.$valid ) {

    } else if ($scope.loginForm.$invalid) {

    }

  };

});