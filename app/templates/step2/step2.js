'use strict';

angular.module('myApp.step2', ['ngRoute', 'ui.bootstrap'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/step2', {
    templateUrl: 'templates/step2/step2.html',
    controller: 'Step2Ctrl'
  });
}])

.controller('Step2Ctrl', [function($scope) {
      //$scope.tabs = [{
      //  slug: 'dashboard',
      //  title: "Dashboard",
      //  content: "Your Dashboard"
      //}, {
      //  slug: 'room-1',
      //  title: "Room 1",
      //  content: "Dynamic content 1"
      //}, {
      //  slug: 'room-2',
      //  title: "Room 2",
      //  content: "Dynamic content 2"
      //}];

      $('.plan').on('click', function() {
        $(this).css('border','1px solid #58b9de');
      });

}]);

