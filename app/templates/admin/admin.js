(function() {
    /****/
    var arrItems = ['make-calls', 'plan-usage', 'billing-history',
                    'call-navigation', 'voicemail', 'plans', 'account-info', 'freebies'];

    arrItems.forEach(function(item){

        $('#'+item).hover(
            function() {
                $('#'+item + ' img').attr('src', '../../img/admin-icons/' + item + '-hover.svg');
                $('#'+item + ' p').css('color', '#BBBEBF');
            }, function () {
                $('#'+item + ' img').attr('src', '../../img/admin-icons/' + item + '-not-selected.svg');
                $('#'+item + ' p').css('color', '#D1D3D4');
            }
        );
        $('#'+item).on('click', function () {
            $('.side-menu ul li').removeClass('active');
            $('#'+item).addClass('active');
            $('#'+item+'.active img').attr('src', '../../img/admin-icons/' + item + '-selected.svg');
            $('#'+item+'.active p').css('color', '#4A4A4A');
            $(this).unbind("mouseenter mouseleave hover");
        });

        setInterval(function(){
            if(!$('#'+item).hasClass('active')){
                $('#'+item + ' img').attr('src', '../../img/admin-icons/' + item + '-not-selected.svg');
                $('#'+item + ' p').css('color', '#D1D3D4');
                $('#'+item).hover(
                    function() {
                        $('#'+item + ' img').attr('src', '../../img/admin-icons/' + item + '-hover.svg');
                        $('#'+item + ' p').css('color', '#BBBEBF');
                    }, function () {
                        $('#'+item + ' img').attr('src', '../../img/admin-icons/' + item + '-not-selected.svg');
                        $('#'+item + ' p').css('color', '#D1D3D4');
                    }
                );
            } else {}
        },750);
    });
})();