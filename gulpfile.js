var gulp = require('gulp');
var less = require('gulp-less');
var uglify = require('gulp-uglify');
var pump = require('pump');
var path = require('path');
var watch = require('gulp-watch');
var cleanCSS = require('gulp-clean-css');
var concatCss = require('gulp-concat-css');
var clean = require('gulp-clean');

gulp.task('compress', function (cb) {
    pump([
            gulp.src([
                'app/login/login.js',
                'app/step1/step1.js'
            ]),
            uglify(),
            gulp.dest('app/scripts')
        ],
        cb
    );
});

gulp.task('clean', function () {
    return gulp.src('app/css/app.css', {read: false})
        .pipe(clean());
});

gulp.task('concat-css',['less','clean'], function () {
    return gulp.src('app/css/*.css')
        .pipe(concatCss("app.css"))
        .pipe(gulp.dest('app/css'));
});

gulp.task('minify-css', function() {
    return gulp.src('app/css/app.css')
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('app'));
});

gulp.task('less',['clean'], function () {
    return gulp.src('app/less/*.less')
        .pipe(less({
            paths: [ path.join(__dirname, 'less', 'includes') ]
        }))
        .pipe(gulp.dest('app/css'));
});

gulp.task('watch', function() {
    gulp.watch('app/less/*.less', ['less','concat-css']);
});

gulp.task('css',['concat-css','minify-css']);
gulp.task('default',['watch']);